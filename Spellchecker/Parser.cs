﻿namespace Spellchecker
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;


    public class Parser
    {
        public static IEnumerable<Prüfwort> WörterIdentifizieren(IEnumerable<string> zeile)
        {
            Regex regex = new Regex(@"\b[a-z,A-Z]\w*\b");
            List<Prüfwort> prüfworter = new List<Prüfwort>();

            int zeileNummer = 1;
            foreach (List<string> matches in zeile.Select(s => regex.Matches(s).OfType<Match>().Select(m => m.Value).ToList()))
            {
                prüfworter.AddRange(matches.Select(w => new Prüfwort(zeileNummer, w)));
                zeileNummer++;
            }

            return prüfworter;
        }
    }
}
