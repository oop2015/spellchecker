﻿namespace Spellchecker
{
    using System.Collections.Generic;

    public class TextProvider
    {
        public static IEnumerable<string> TextEinlessen(string textdateiFilename)
        {
            return System.IO.File.ReadAllLines(textdateiFilename);
        }
    }
}
