﻿namespace Spellchecker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Wörterbuch
    {
        private readonly List<string> wörter;

        public Wörterbuch(IEnumerable<string> wörter)
        {
            this.wörter = wörter.ToList();
        }

        public IEnumerable<Prüfwort> Spellcheck(IEnumerable<Prüfwort> wörterZuPrüfen)
        {
            var fehler = (from p in wörterZuPrüfen
                          where !this.wörter.Contains(p.Wort, StringComparer.InvariantCultureIgnoreCase)
                          select p).ToList();
            return fehler;
        }
    }
}
