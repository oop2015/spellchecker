﻿namespace Spellchecker
{
    public class Prüfwort
    {
        public Prüfwort(int zeileNummer, string wort)
        {
            this.Wort = wort;
            this.ZeileNummer = zeileNummer;
        }

        public string Wort { get; private set; }

        public int ZeileNummer { get; private set; }
    }
}
