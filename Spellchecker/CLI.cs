﻿namespace Spellchecker
{
    using System;
    using System.Collections.Generic;

    public class CLI
    {
        public static string TextdateiNameErmitteln(string[] args)
        {
            return args[0];
        }

        public static void FehlerAusgeben(IEnumerable<string> fehlern)
        {
            foreach (var s in fehlern)
            {
                Console.WriteLine(s);
            }
        }
    }
}
