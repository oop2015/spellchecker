﻿namespace Spellchecker
{
    using System;
    using System.Collections.Generic;

    class Program
    {
        // a comment just for testing Git functionality ...
        static void Main(string[] args)
        {
            var wörterbuch = WörterbuchProvider.WörtebuchEinlessen();
            var prüfworte = Prüfworte_laden(args.Length > 0 ? args : new [] {"data.txt" });
            var fehlern = wörterbuch.Spellcheck(prüfworte);
            var formatierteFehlern = AusgabeFormatierer.GruppierteFormatierung(fehlern);
            CLI.FehlerAusgeben(formatierteFehlern);
            Console.ReadLine();
        }

        private static IEnumerable<Prüfwort> Prüfworte_laden(string[] args)
        {
            var textDateiFilename = CLI.TextdateiNameErmitteln(args);
            var textZuPrüfen = TextProvider.TextEinlessen(textDateiFilename);
            var prüfworte = Parser.WörterIdentifizieren(textZuPrüfen);

            return prüfworte;
        }
    }
}
