﻿namespace Spellchecker
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class AusgabeFormatierer
    {
        public static IEnumerable<string> StandardFormatierung(IEnumerable<Prüfwort> prüfwörter)
        {
            return prüfwörter.Select(p => p.Wort + ", " + p.ZeileNummer).ToList();
        }

        public static IEnumerable<string> GruppierteFormatierung(IEnumerable<Prüfwort> prüfwörter)
        {
            List<string> formatierteAusgabe = new List<string>();
            List<Prüfwort> prüfwörterList = prüfwörter.ToList();

            StringBuilder sb = new StringBuilder();
            Prüfwort altePrüfwort = prüfwörterList.First();

            sb.Append(altePrüfwort.ZeileNummer).Append(": ").Append(altePrüfwort.Wort);

            for (int i = 1; i < prüfwörterList.Count; i++)
            {
                var p = prüfwörterList[i];
                if (p.ZeileNummer == altePrüfwort.ZeileNummer)
                {
                    sb.Append(", ").Append(p.Wort);
                }
                else
                {
                    formatierteAusgabe.Add(sb.ToString());
                    sb = new StringBuilder();
                    sb.Append(p.ZeileNummer).Append(": ").Append(p.Wort);
                }

                altePrüfwort = p;
            }

            formatierteAusgabe.Add(sb.ToString());

            return formatierteAusgabe;
        }
    }
}
