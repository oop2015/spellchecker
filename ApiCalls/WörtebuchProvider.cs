﻿namespace Spellchecker
{
    public class WörtebuchProvider
    {
        public static Wörtebuch WörtebuchEinlessen()
        {
            var zeilen = System.IO.File.ReadAllLines("Woertebuch.txt");
            return new Wörtebuch(zeilen);
        }
    }
}
