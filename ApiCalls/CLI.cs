﻿namespace Spellchecker
{
    using System;
    using System.Collections.Generic;

    public class CLI
    {
        public static string TextdateiNameErmitteln(string[] args)
        {
            return args[0];
        }

        public static void FehlerAusgeben(IEnumerable<Prüfwort> fehlern)
        {
            foreach (var p in fehlern)
            {
                Console.WriteLine(p.Wort + ", " + p.ZeileNummer);
            }
        }
    }
}
